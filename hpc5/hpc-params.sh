#!/bin/bash -l

#################### HPC prepration phase ##################

#PBS -l walltime=23:59:00
#PBS -l nodes=1:ppn=28
#PBS -m bea
#PBS -M glenn.daneels@uantwerpen.be

cd $PBS_O_WORKDIR

module load leibniz/2017c
module load Python/2.7.14-intel-2017c
module load parallel/20170322
