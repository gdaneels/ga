#!/bin/bash -l

source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh
#source config-local.sh

if [ -d "${RUNNING_DIR}" ]; then
        rm -r ${RUNNING_DIR}
        echo "Removed ${RUNNING_DIR}."
else
        echo "${RUNNING_DIR} does not exist, so no removal."
fi

if [ -d "${OUTPUT_DIR}" ]; then
	rm -r ${OUTPUT_DIR}
	echo "Removed ${OUTPUT_DIR}."
else
	echo "${OUTPUT_DIR} does not exist, so no removal."
fi

if [ -d "${FAILED_DIR}" ]; then 
	rm -r ${FAILED_DIR}
        echo "Removed ${FAILED_DIR}."
else
        echo "${FAILED_DIR} does not exist, so no removal."
fi
