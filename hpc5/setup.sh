#!/bin/bash -l

ENVIRONMENT="$1"

if [ "$ENVIRONMENT" == "local" ]; then
	echo "Preparing the local environment."
        sed -i "s:^source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh:#source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh:g" ./*.sh
	sed -i "s:^#source config-local.sh:source config-local.sh:g" ./*.sh
elif [ "$ENVIRONMENT" == "hpc" ]; then
	echo "Preparing the HPC environment."
        sed -i "s:^#source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh:source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh:g" ./*.sh
        sed -i "s:^source config-local.sh:#source config-local.sh:g" ./*.sh
fi
