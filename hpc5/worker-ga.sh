#!/bin/bash -l

source /user/antwerpen/202/vsc20268/ga5/hpc5/config-hpc.sh
#source config-local.sh

sleep $[ ( $RANDOM % 300 )  + 1 ]s

#################### Preparation phase #######################

RANDOM_ID="$(uuidgen)"
WORKER_NAME="worker-5-`date '+%Y-%m-%d-%H-%M-%S'`-${RANDOM_ID}"

echo "Hi, I'm your worker named ${WORKER_NAME}."
echo ""

WORKER_LOCATION="${WORKING_LOCATION}/${WORKER_NAME}"
SIMULATOR_LOCATION="${WORKER_LOCATION}/ga5"

mkdir -p "${WORKER_LOCATION}" # make worker
mkdir -p $OUTPUT_DIR # make if not exists
mkdir -p $FAILED_DIR # make if not exists
mkdir -p $RUNNING_DIR # make if not exists
cp -r ${LATEST_SIMULATOR_LOCATION} ${WORKER_LOCATION} # copy latest simulator to worker
# delete all unnecessary files
find ${WORKER_LOCATION} -name \*.pyc -type f -delete
rm -rf ${WORKER_LOCATION}/ga5/examples
rm -rf ${WORKER_LOCATION}/ga5/experiments/
rm -rf ${WORKER_LOCATION}/ga5/hpc/
rm -rf ${WORKER_LOCATION}/ga5/hpc2/
rm -rf ${WORKER_LOCATION}/ga5/hpc3/
rm -rf ${WORKER_LOCATION}/ga5/hpc4/
rm -rf ${WORKER_LOCATION}/ga5/hpc5/
rm -rf ${WORKER_LOCATION}/ga5/hpc6/
rm -rf ${WORKER_LOCATION}/ga5/commands.txt
rm -rf ${WORKER_LOCATION}/ga5/information.txt
rm -rf ${WORKER_LOCATION}/ga5/simulator/README.md
rm -rf ${WORKER_LOCATION}/ga5/simulator/requirements.txt
rm -rf ${WORKER_LOCATION}/ga5/p_files/*
rm -rf ${WORKER_LOCATION}/ga5/topologies/*
rm -rf ${WORKER_LOCATION}/ga5/settings/*
rm -rf ${WORKER_LOCATION}/ga5/simulator/bitbucket-pipelines.yml
rm -rf ${WORKER_LOCATION}/ga5/.git

################### Experiment phase ########################

for CONFIGURATION_FILE in `find "${INPUT_DIR}" -name "*.json" -type f`
do
	echo "Checking ${CONFIGURATION_FILE}..."

	FILENAME="$(basename ${CONFIGURATION_FILE})" # discard the directories in front of it
	echo "	Filename = ${FILENAME}"
	EXPERIMENT_NAME="${FILENAME%.json}" # drop the .json, keep the experiment file
	echo "	Experiment name = ${EXPERIMENT_NAME}"
    FULLPATH="${INPUT_DIR}/${FILENAME}"
	echo "	Full path = ${FULLPATH}"
	LOCK="${OUTPUT_DIR}/${EXPERIMENT_NAME}" # name of the lock file
	echo "	Lock file = ${LOCK}"
	FAILED_LOCK="${FAILED_DIR}/${EXPERIMENT_NAME}/job.lock" # name of the failed lock
	echo "	Failed lock file = ${FAILED_LOCK}"

	if ! mkdir "${LOCK}" &>/dev/null; then
		echo "	Locked? Yes."
		continue # if the lock already exists or the failed lock exists, go to next experiment
	fi
	echo "	Locked? No."

	#install -D /dev/null ${LOCK} # creates lock file and all directories
        echo "	Locked experiment."

	cd ${SIMULATOR_LOCATION} # go in ga directory
	echo "	Starting experiment."

    #rm -f ${SIMULATOR_LOCATION}/result*.dat # remove everything from before
    rm -f ${SIMULATOR_LOCATION}/solutions/*.json # remove everything from before
    rm -f ${SIMULATOR_LOCATION}/topologies/*.json # remove everything from before

	FULLPATH_OUTPUT="${OUTPUT_DIR}/${EXPERIMENT_NAME}"
	LOGFILE="${FULLPATH_OUTPUT}/simulator_output.txt"
	GA_LOGFILE="${FULLPATH_OUTPUT}/ga_output.txt"
	ERRORFILE="${FULLPATH_OUTPUT}/error.log"
	touch ${LOGFILE} # make the logfile
	touch ${GA_LOGFILE} # make the logfile

	RUNNING_FILE="${RUNNING_DIR}/${EXPERIMENT_NAME}.exp.txt"
	touch ${RUNNING_FILE} # make the file that indicates that the experiment is running

	cd ${SIMULATOR_LOCATION}/simulator/bin # go in 6tisch simulator directory

    module load Python/2.7.16-intel-2019b

    # 1) run the python simulator to generate a topology
	python runSim.py --json=${INPUT_DIR}/${FILENAME} --genTopology=1 --topologyPath=${SIMULATOR_LOCATION}/topologies 2>${ERRORFILE} 1>${LOGFILE}

    module load Python/3.7.4-intel-2019b
    module load Gurobi/8.1.1
    export PYTHONPATH=$EBROOTGUROBI/lib/python3.7_utf32:$PYTHONPATH

    if [ ! -s ${ERRORFILE} ]; then
        cd ${SIMULATOR_LOCATION} # go back to ga simulator directory
        # 2) run the GA and solve it
        timeout $1s python ga-fast.py -i ${INPUT_DIR}/${FILENAME} -l INFO 2>${ERRORFILE} 1>${GA_LOGFILE}
        if [ $? -eq 124 ]; then
            echo "ga.py got time-outed." > ${ERRORFILE}
        fi
        cd ${SIMULATOR_LOCATION}/simulator/bin # go in 6tisch simulator directory
        module load Python/2.7.16-intel-2019b
        if [ ! -s ${ERRORFILE} ]; then
            # 3) run the simulator again, now with the JSON experiment file and the ILP file altered by GUROBI
            python runSim.py --json=${INPUT_DIR}/${FILENAME} --ilpfile="${SIMULATOR_LOCATION}/topologies/simulator-topology.json" --ilpschedule="${SIMULATOR_LOCATION}/solutions/ga-schedule.json" 2>${ERRORFILE} 1>${LOGFILE}
        fi
    fi

	rm -f ${RUNNING_FILE} # remove the file that indicates that experiment is running

	IDFILE="${FULLPATH_OUTPUT}/${WORKER_NAME}.id.txt"
	touch ${IDFILE}

    # copy the configuration file
	cp ${CONFIGURATION_FILE} ${FULLPATH_OUTPUT}

	if [ -f "${SIMULATOR_LOCATION}/solutions/ga.json" ]; then
		cp ${SIMULATOR_LOCATION}/solutions/ga.json ${FULLPATH_OUTPUT}/ga.json # copy the ILP solution json file
		rm -f ${SIMULATOR_LOCATION}/solutions/ga.json # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/solutions/ga-evolution.eps" ]; then
		cp ${SIMULATOR_LOCATION}/solutions/ga-evolution.eps ${FULLPATH_OUTPUT}/ga-evolution.eps # copy the ILP solution json file
		rm -f ${SIMULATOR_LOCATION}/solutions/ga-evolution.eps # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/solutions/ga-schedule.json" ]; then
		cp ${SIMULATOR_LOCATION}/solutions/ga-schedule.json ${FULLPATH_OUTPUT}/ga-schedule.json # copy the ILP solution json file
		rm -f ${SIMULATOR_LOCATION}/solutions/ga-schedule.json # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/solutions/visualization-ga.html" ]; then
		cp ${SIMULATOR_LOCATION}/solutions/visualization-ga.html ${FULLPATH_OUTPUT}/visualization-ga.html # copy the ILP solution json file
		rm -f ${SIMULATOR_LOCATION}/solutions/visualization-ga.html # remove everything from before
	fi
    if [ -f "${SIMULATOR_LOCATION}/topologies/simulator-topology.json" ]; then
		cp ${SIMULATOR_LOCATION}/topologies/simulator-topology.json ${FULLPATH_OUTPUT}/simulator-topology.json
		rm -f ${SIMULATOR_LOCATION}/topologies/simulator-topology.json # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/settings/settings.json" ]; then
		cp ${SIMULATOR_LOCATION}//settings/settings.json ${FULLPATH_OUTPUT}/settings.json
		rm -f ${SIMULATOR_LOCATION}/settings/settings.json # remove everything from before
	fi
		if [ -f "${SIMULATOR_LOCATION}/simulator/bin/consumption.json" ]; then
		cp ${SIMULATOR_LOCATION}/simulator/bin/consumption.json ${FULLPATH_OUTPUT}/consumption.json # copy the ILP solution json file
		rm -f ${SIMULATOR_LOCATION}/simulator/bin/consumption.json # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/simulator/bin/runSim.log" ]; then
		cp ${SIMULATOR_LOCATION}/simulator/bin/runSim.log ${FULLPATH_OUTPUT}/runSim.log # copy the simulator's log
		rm -f ${SIMULATOR_LOCATION}/simulator/bin/runSim.log # remove everything from before
	fi
	if [ -f "${SIMULATOR_LOCATION}/simulator/bin/simData/output_cpu0.dat" ]; then # copy the results if they exist
        cp "${SIMULATOR_LOCATION}/simulator/bin/simData/output_cpu0.dat" ${FULLPATH_OUTPUT}/output_cpu0.dat
        rm -f "${SIMULATOR_LOCATION}/simulator/bin/simData/output_cpu0.dat" # remove everything from before
    fi

	if [ -s ${ERRORFILE} ]; then
		echo "	Stopping FAILED experiment."
		install -D /dev/null ${FAILED_LOCK} # creates lock file and all directories
	else
		echo "	Stopping SUCCESSFUL experiment."
	fi
done
