import random
import json
import os

random.seed(7)

ITERATIONS = 1
INPUT_DIR = 'input'
MAX_SEED = 10000
random_seeds = []

##### SIMULATOR SPECIFIC SETTINGS #####

expLength = 300 # seconds
cooldownLength = 60 # seconds

simulator_config = {}
simulator_config['numRuns'] = 1
simulator_config['convergeFirst'] = 1
simulator_config['numChans'] = 3 # take only two channels to limit the total number of slots
simulator_config['numMotes'] = 8 # number of motes
simulator_config['topology'] = 'random' # we only consider a random topology
simulator_config['nrMinimalCells'] = 0 # we only want one 1 minimal cell, however it won't really be used in the ILP case
simulator_config['trafficGenerator'] = 'ilp' # the ilp setting deals with the correct ILP traffic
simulator_config['sporadicTraffic'] = 0 # to be sure, set it to 0. we do not want sporadic traffic.
simulator_config['changeParent'] = 0 # we do not change parents
simulator_config['backoffMinExp'] = 1 # we do not have SHARED cells anymore (except for the minimal ones), so we do not care
simulator_config['backoffMaxExp'] = 1 # we do not have SHARED cells anymore (except for the minimal ones), so we do not care
simulator_config['sf'] = 'ilp' # we only want to use the ILP SF.
simulator_config['minCellsMSF'] = 1 # is ignored.
simulator_config['packetSize'] = 127 # maximum packet size
simulator_config['subGHz'] = 1 # yes, go subGHz
simulator_config['individualModulations'] = 1 # we have modulations per link
simulator_config['pkPeriodVar'] = 0 # important to set to 0 to have a nice equally sent packet distribution

simulator_config['cooldown'] = cooldownLength / 0.005

simulator_config['modulationConfig'] = 'ILP5msFR'
simulator_config['slotDuration'] = 0.005 # will be replaced
simulator_config['slotframeLength'] = 40 # will be replaced
simulator_config['numCyclesPerRun'] = int(expLength / 0.005 * 40) # will be replaced
simulator_config['noNewInterference'] = 1 # will be replaced
simulator_config['noPropagationLoss'] = 0 # will be replaced

simulator_config['settlingTime'] = 120 # in seconds
simulator_config['maxToConverge'] = 6060 # in seconds

simulator_config['stableNeighbors'] = 7

# TODO SEED STILL HAS TO BE TAKEN FROM THE GLOBAL SETTINGS FILE NOW


##### GA SPECIFIC SETTINGS #####


DEFAULT_SETTINGS = 'default-settings/settings.json'
config = {}
with open(DEFAULT_SETTINGS) as json_file:
    config = json.load(json_file)
    config["simulator"] = simulator_config

##### SETTINGS FILE #####

def getUniqueSeed():
    seed = random.randint(0, MAX_SEED)
    while seed in random_seeds:
        seed = random.randint(0, MAX_SEED)
    random_seeds.append(seed)
    return seed

def makeFile(seed):
    if not os.path.exists(INPUT_DIR):
        os.mkdir(INPUT_DIR)
    nameFile = '%s/ga_seed_%d.json' % (INPUT_DIR, seed)
    with open(nameFile, 'w') as outfile:
        config["seed"] = seed
        json.dump(config, outfile)

if __name__ == "__main__":
    seeds = [getUniqueSeed() for x in range(ITERATIONS)]
    cnt = 0
    while cnt < ITERATIONS:
        makeFile(seeds[cnt])
        cnt += 1
