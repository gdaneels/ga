#!/bin/bash -l

source /user/antwerpen/202/vsc20268/ga2/hpc2/config-hpc.sh
#source config-local.sh

COUNT_INPUTS=$(ls -1q ${INPUT_DIR}/*.json | wc -l)
COUNT_OUTPUTS=$(find ${OUTPUT_DIR} -name "ga.json" -type f | wc -l)
COUNT_FAILED=$(find ${FAILED_DIR} -name "job.lock" -type f | wc -l)
COUNT_RUNNING=$(find ${RUNNING_DIR} -name "*.exp.txt" -type f | wc -l)

echo "Failed experiments: "
echo $(find ${FAILED_DIR} -name "job.lock" -type f)
echo ""

echo "Running experiments: "
echo $(find ${RUNNING_DIR} -name "*.exp.txt" -type f)
echo ""

echo "${COUNT_RUNNING} job(s) running."
echo "${COUNT_OUTPUTS}/${COUNT_INPUTS} jobs finished."
echo "${COUNT_FAILED}/${COUNT_INPUTS} jobs failed."
