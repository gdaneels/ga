#!/bin/bash -l

ORIG_INPUT_DIR="/user/antwerpen/202/vsc20268/ga/experiments/input"
SCRATCH_INPUT_DIR="/scratch/antwerpen/202/vsc20268/ga"

if [ ! -d "$ORIG_INPUT_DIR" ]; then
  echo >&2 "Input directory does not exist."
  exit 1
fi

cp -r "${ORIG_INPUT_DIR}"  "${SCRATCH_INPUT_DIR}"
echo "Input directory copied successfully."

