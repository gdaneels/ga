#!/bin/bash -l

#################### Worker preparation phase ##############

WORKING_LOCATION="/scratch/antwerpen/202/vsc20268"

LATEST_SIMULATOR_LOCATION="${VSC_HOME}/ga"
if [ ! -d "$LATEST_SIMULATOR_LOCATION" ]; then
  echo >&2 "Original simulator does not exist."
  exit 1
fi

INPUT_DIR="/scratch/antwerpen/202/vsc20268/ga/input"
OUTPUT_DIR="/scratch/antwerpen/202/vsc20268/ga/output"
FAILED_DIR="/scratch/antwerpen/202/vsc20268/ga/failed"
RUNNING_DIR="/scratch/antwerpen/202/vsc20268/ga/running"

if [ ! -d "$INPUT_DIR" ]; then
  echo >&2 "Input directory does not exist."
  exit 1
fi
