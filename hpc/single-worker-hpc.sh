#!/bin/bash -l

#################### HPC prepration phase ##################

#PBS -l walltime=00:59:00
#PBS -l pvmem=4g
#PBS -l nodes=1:ppn=1
#PBS -m bea
#PBS -M glenn.daneels@uantwerpen.be

cd $PBS_O_WORKDIR

module load leibniz/2018b
module load Python/2.7.15-intel-2018b
module load parallel/20170322

source worker.sh
