#!/bin/bash -l

#source hpc-params.sh

parallel --no-notice --progress -n0 ./worker.sh ::: {1..2}
