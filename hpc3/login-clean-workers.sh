#!/bin/bash -l

source /user/antwerpen/202/vsc20268/ga3/hpc3/config-hpc.sh
#source config-local.sh

for CONFIGURATION_FILE in `find "${WORKING_LOCATION}" -name "worker-3-*" -type d`
do
	echo "Removing ${CONFIGURATION_FILE}"
	rm -rf ${CONFIGURATION_FILE}
done
