#!/bin/bash -l

#################### Worker preparation phase ##############

WORKING_LOCATION="/scratch/antwerpen/202/vsc20268"

LATEST_SIMULATOR_LOCATION="${VSC_HOME}/ga3"
if [ ! -d "$LATEST_SIMULATOR_LOCATION" ]; then
  echo >&2 "Original simulator does not exist."
  exit 1
fi

INPUT_DIR="/scratch/antwerpen/202/vsc20268/ga3/input"
OUTPUT_DIR="/scratch/antwerpen/202/vsc20268/ga3/output"
FAILED_DIR="/scratch/antwerpen/202/vsc20268/ga3/failed"
RUNNING_DIR="/scratch/antwerpen/202/vsc20268/ga3/running"

if [ ! -d "$INPUT_DIR" ]; then
  echo >&2 "Input directory does not exist."
  exit 1
fi
