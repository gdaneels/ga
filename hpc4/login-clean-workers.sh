#!/bin/bash -l

source /user/antwerpen/202/vsc20268/ga4/hpc4/config-hpc.sh
#source config-local.sh

for CONFIGURATION_FILE in `find "${WORKING_LOCATION}" -name "worker-4-*" -type d`
do
	echo "Removing ${CONFIGURATION_FILE}"
	rm -rf ${CONFIGURATION_FILE}
done
