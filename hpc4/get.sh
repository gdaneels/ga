#!/bin/bash

ROOT_RESULTS_DIR="../output/"
mkdir -p $ROOT_RESULTS_DIR

RESULTS_DIR="${ROOT_RESULTS_DIR}/results-`date '+%Y-%m-%d-%H-%M-%S'`"

rsync -avzrP vsc20268@login.hpc.uantwerpen.be:/scratch/antwerpen/202/vsc20268/ga4/output/* $RESULTS_DIR

