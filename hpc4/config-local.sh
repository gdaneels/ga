#!/bin/bash -l

#################### Worker preparation phase ##############

WORKING_LOCATION="/home/openwsn"

LATEST_SIMULATOR_LOCATION="/home/openwsn/6tisch_resf"
if [ ! -d "$LATEST_SIMULATOR_LOCATION" ]; then
  echo >&2 "Original simulator does not exist."
  exit 1
fi

# these should not be placed inside the simulator directory
INPUT_DIR="/home/openwsn/input"
OUTPUT_DIR="/home/openwsn/output"
FAILED_DIR="/home/openwsn/failed"
RUNNING_DIR="/home/openwsn/running"

if [ ! -d "$INPUT_DIR" ]; then
  echo >&2 "Input directory does not exist."
  exit 1
fi
