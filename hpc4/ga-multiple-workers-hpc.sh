#!/bin/bash -l

#################### HPC prepration phase ##################

#PBS -l walltime=23:59:00
#PBS -l nodes=1:ppn=28
#PBS -m bea
#PBS -M glenn.daneels@uantwerpen.be

cd $PBS_O_WORKDIR

module load parallel/20180422

SUBJOB_MAX_WALLTIME_S=$(( 1*30000 ))

parallel --no-notice --progress -n0 ./worker-ga.sh $SUBJOB_MAX_WALLTIME_S ::: {1..28}